package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
)

type Login struct {
	Email    string
	Password string
}

func login(w http.ResponseWriter, body []byte) {
	var login Login
	err := json.Unmarshal(body, &login)
	if err != nil {
		fmt.Fprintf(w, buildJsonError(err.Error()))
		return
	}
	hash := sha256.New()
	hash.Write([]byte(login.Password))
	login.Password = fmt.Sprintf("%x", hash.Sum(nil))
	var id int
	var accountType string = "user"
	err = db.QueryRow("SELECT id FROM users WHERE email=? AND password=?", login.Email, login.Password).Scan(&id)
	if err == sql.ErrNoRows {
		accountType = "business"
		err = db.QueryRow("SELECT id FROM businesses WHERE email=? AND password=?", login.Email, login.Password).Scan(&id)
		if err == sql.ErrNoRows {
			http.Error(w, buildJsonError("invalid username or password"), 400)
			return
		}
	}
	fmt.Fprintf(w, "{\"success\": true, ")

	//Set jwt
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		id:          id,
		accountType: accountType,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, _ := token.SignedString(jwtKey)

	fmt.Fprintf(w, "\"token\": \""+tokenString+"\"}")
	return
}
