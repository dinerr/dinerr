package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"html"
	"net/http"
	"time"
)

type NewBusiness struct {
	Name     string
	Email    string
	City     string
	State    string
	ZipCode  string
	Password string
}

const maxBusinesses int = 100

func listBusinesses(w http.ResponseWriter, limit int, offset int, search string, cat int) {
	//TODO: add cat later
	if limit > maxBusinesses {
		limit = maxBusinesses
	}
	businesses := make([]int, 0)
	if search != "" {
		rows, err := db.Query("SELECT id FROM businesses WHERE name LIKE '%" + html.EscapeString(search) + "%'")
		if err == sql.ErrNoRows {
			fmt.Fprintf(w, buildJsonError("no businesses found"), 400)
			return
		}
		if err != nil {
			fmt.Fprintf(w, buildJsonError("Internal server error"), 500)
			return
		}
		defer rows.Close()
		businesses := make([]int, 0)
		for rows.Next() {
			var name int
			if err := rows.Scan(&name); err != nil {
				fmt.Fprintf(w, buildJsonError("no businesses found"), 400)
				return
			}
			businesses = append(businesses, name)
		}
	}
	fmt.Fprintf(w, "{ \"businesses\": [")
	for i := offset; i < offset+limit || i < len(businesses)+offset; i++ {
		if search != "" {
			listBusiness(w, businesses[i], false)
		} else {
			listBusiness(w, i, false)
		}
		if i != offset+limit-1 || i != len(businesses)+offset-1 {
			fmt.Fprintf(w, ", ")
		}
	}
	fmt.Fprintf(w, "]}")
}

func createBusiness(w http.ResponseWriter, body []byte) {
	var newBusiness NewBusiness
	err := json.Unmarshal(body, &newBusiness)
	if err != nil {
		http.Error(w, buildJsonError("invalid json request"), 400)
		return
	}
	if !validateEmail(newBusiness.Email) {
		http.Error(w, buildJsonError("invalid email address"), 400)
		return
	}
	if newBusiness.Name == "" {
		http.Error(w, buildJsonError("invalid business Name"), 400)
		return
	}
	if newBusiness.City == "" {
		http.Error(w, buildJsonError("invalid City Name"), 400)
		return
	}
	if newBusiness.State == "" {
		http.Error(w, buildJsonError("invalid State"), 400)
		return
	}
	if len(newBusiness.ZipCode) != 5 {
		http.Error(w, buildJsonError("invalid zip code"), 400)
		return
	}
	if !validatePassword(newBusiness.Password) {
		http.Error(w, buildJsonError("invalid password"), 400)
		return
	}

	err = db.QueryRow("SELECT Email FROM users where Email=?", newBusiness.Email).Scan(&newBusiness.Email)
	if err != sql.ErrNoRows {
		http.Error(w, buildJsonError("business already exists"), 400)
		return
	}

	err = db.QueryRow("SELECT Email FROM businesses where Email=?", newBusiness.Email).Scan(&newBusiness.Email)
	if err != sql.ErrNoRows {
		http.Error(w, buildJsonError("a user account already exists with this Email"), 400)
		return
	}

	statement, err := db.Prepare("INSERT INTO businesses (name, email, city, state, zipcode) VALUES (?, ?, ?, ? ,?)")

	hash := sha256.New()
	hash.Write([]byte(newBusiness.Password))
	newBusiness.Password = fmt.Sprintf("%x", hash.Sum(nil))

	statement.Exec(newBusiness.Name, newBusiness.Email, newBusiness.City, newBusiness.State, newBusiness.ZipCode)

	fmt.Fprintf(w, "{\"success\": true, ")

	row := db.QueryRow("SELECT id FROM users WHERE email=?", newBusiness.Email)
	var id int
	row.Scan(&id)

	//Set jwt
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		id:          id,
		accountType: "business",
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, _ := token.SignedString(jwtKey)

	fmt.Fprintf(w, "\"token\": \""+tokenString+"\"}")
	return
}

func listBusiness(w http.ResponseWriter, id int, genErrors bool) {
	var name, city, state string
	err := db.QueryRow("SELECT name, city, state FROM businesses where id = ?", id).Scan(&name, &city, &state)
	if err != nil {
		if genErrors {
			http.Error(w, buildJsonError("user not found"), 400)
		}
		return
	}

	fmt.Fprintf(w, "{\"name\": \""+name+"\",\r\n"+
		"\"city\": \""+city+"\",\r\n"+
		"\"state\": \""+state+"\"}")

	return
}

func editBusiness(w http.ResponseWriter, id int) {
	//Todo: add this endpoint
}

func deleteBusiness(w http.ResponseWriter, id int) {
	//todo: add this endpoint
}
