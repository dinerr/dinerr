## Testing

To run tests, simply run `go test -v`

## Installation

First, ensure the Go programming language is installed on your computer. For Windows, there are msi files on golang.org. For Debian based linux distributions such as Kali and Ubuntu, simply run `sudo apt install golang`

Add GOPATH={your home directory}/go as and environment variable on your machine. On Debian based Linux builds, this is generally done by adding `export GOPATH="${HOME}/go"` to your `.bashrc` file.

To build the server, run `build.sh`

The main binary has 2 optional command line arguments. For usage, run `./main -h` Please read the usage before running main.

## REST API Docs

Note: All responses may output an error.

### Users: Logging In
`POST /api/user/login`
Request body:
```json
{
  "email": "emilyk",
  "password": "1234abcd"
}
```
Response body:
```json
 {}
```
Notes: I don't really know what this will return when using sessions. ~Emily
<hr>

### Users: Create an Account
`POST /api/user/register`
Request body:
```json
{
  "email": "test@example.com",
  "password": "G00dPass!",
  "firstName": "Emily",
  "lastName": "Kauffman",
  "city": "Pittsburgh",
  "state": "PA"
}
```
Request response:
```json
{
  "success": true
}

```
<hr>

### Users: Get a profile based on ID
`GET /api/user/id/:id`
Request body:
`none`
Request response:
```json
{
  "firstName": "Emily",
  "lastName": "Kauffman",
  "city": "Pittsburgh",
  "state": "PA"
}
```
<hr>

### Users: Get a profile based on currently logged in user
`GET /api/user/me`
Request body:
`none`
Request response:
```json
{
  "firstName": "Emily",
  "lastName": "Kauffman",
  "city": "Pittsburgh",
  "state": "PA"
}
```
<hr>

### Businesses: Get all businesses
`GET /api/business`
Request body:
`none`
Request response:
```json
[
  {
    "id": 123,
    "name": "Grist House",
    ...
  },
  {
    "id": 124,
    "name": "Eleventh Hour",
    ...
  }
]
```
<hr>

### Businesses: Get business details by ID
`GET /api/business/id/:id`
Request body:
`none`
Request response:
```json
[
  {
    "id": 123,
    "name": "Grist House",
    ...
  }
]
```
<hr>

### Businesses: Get all businesses with query filters
`GET /api/business`

Request body:
`none`

Request params:
- `limit` - used in pagination, limit # of returned results
- `offset` - used in pagination, where to start results
- `search` - string used to search for businesses by name
- `cat` - category ID of which it's being filtered (ex. 1 = restaurant, 2 = food truck, etc)

Request response:
```json
[
  {
    "id": 123,
    "name": "Grist House",
    ...
  }
]
```

Example:
`GET /api/business?limit=10&offset=0&search=grist&cat=2`
This would search for all businesses that have "grist" in the name, are of a category ID two, and indicates that it should return the first ten results (offset 0, limit 10).
<hr>

### Businesses: Create new business
`POST /api/business`
Request body:
```json
{
  "name": "Grist House",
  "city": "Pittsburgh",
  "state": "PA",
  ...
}

```

Request response:
```json
{
  "success": true
}
```
<hr>

### Businesses: Delete a business
`DELETE /api/business/id/:id`
Request body:
`none`
Request response:
```json
{
  "success": true
}
```
<hr>

### Businesses: Edit a business
`PUT /api/business/id/:id`
Request body:
```json
{
  "name": "Grist House Brewery",
  ...
}

```
Request response:
```json
{
  "success": true
}
```
<hr>