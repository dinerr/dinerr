package main

import (
	"net/mail"
	"strings"
	"unicode"
)

func validateEmail(email string) bool {
	if strings.ContainsAny(email, "<> ") {
		return false
	}
	_, err := mail.ParseAddress(email)
	if err != nil {
		return false
	}
	return true
}

func validatePassword(password string) bool {
	if len(password) < 8 {
		return false
	}
	var num, special, upper bool
	for _, c := range password {
		switch {
		case unicode.IsNumber(c):
			num = true
		case unicode.IsUpper(c):
			upper = true
		case unicode.IsSymbol(c) || unicode.IsPunct(c):
			special = true
		}
	}
	return num && special && upper
}
