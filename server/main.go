package main

import (
	"database/sql"
	"flag"
	"github.com/dgrijalva/jwt-go"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http"
	"os"
	"strings"
)

type myMux struct{}

type Claims struct {
	id          int
	accountType string
	jwt.StandardClaims
}

var mux = &myMux{}
var frontend string
var db *sql.DB

//TODO: This key is VERY insecure. Only use for prototyping
var jwtKey = []byte("insecurejwtkey")

func serveStatic(w http.ResponseWriter, r *http.Request, path string) {
	if !strings.Contains(path, "static") {
		path = "/"
	}
	f, e := os.Open(frontend + path)
	if e != nil {
		http.NotFound(w, r)
		return
	}
	defer f.Close()
	http.ServeFile(w, r, frontend+path)
	return
}

func (mux *myMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	//Serve static files
	firstDirectory := strings.Split(r.URL.Path, "/")[1]
	if firstDirectory == "api" {
		handleAPI(w, r, r.URL.Path)
	} else {
		serveStatic(w, r, r.URL.Path)
	}
	return
}

func setupSQL(databaseName string) {
	var err error
	db, err = sql.Open("sqlite3", databaseName)
	if err != nil {
		log.Fatal(err)
	}
	//build user table
	statement, err := db.Prepare("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, email TEXT, password TEXT, firstname TEXT, lastname TEXT, city TEXT, state TEXT)")
	if err != nil {
		log.Fatal(err)
	}
	statement.Exec()
	//build business table
	statement, err = db.Prepare("CREATE TABLE IF NOT EXISTS businesses (id INTEGER PRIMARY KEY, name TEXT, email TEXT, city TEXT, state TEXT, zipcode TEXT, password TEXT)")
	if err != nil {
		log.Fatal(err)
	}
	statement.Exec()
	//build posts
	statement, err = db.Prepare("CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY, imgsrc TEXT, businessid INTEGER, description TEXT, createdat INTEGER, updatedat INTEGER)")
	if err != nil {
		log.Fatal(err)
	}
	statement.Exec()
}

// If using GoLand as an IDE, pass one command line argument.
func main() {
	ideaFlag := flag.Bool("idea", false, "Use this only if using Goland")
	testingFlag := flag.Bool("test", false, "If testing use this flag")
	flag.Parse()

	var databaseName, port string
	if *testingFlag {
		databaseName = "test.db"
		port = "3000"
	} else {
		databaseName = "dinerr.db"
		port = "80"
	}

	if *ideaFlag {
		frontend = "frontend/build"
	} else {
		frontend = "../frontend/build"
	}

	setupSQL(databaseName)

	log.Println("Starting server on port " + port + ".")
	log.Fatal(http.ListenAndServe(":"+port, mux))
}
