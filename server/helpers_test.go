// This file is specifically for functions used to help tests - place no tests here

package main

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
)

type errorReader int

var emptyByte = make([]byte, 0, 0)

func fail(t *testing.T, expected, actual interface{}) {
	t.Fatal("\r\nActual: ", actual, "\r\nExpected: ", expected)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	mux.ServeHTTP(rr, req)
	return rr
}

func generateGoodReg() Reg {
	return Reg{
		Password:  "Str0ngP4ssw0rd!",
		FirstName: "John",
		LastName:  "Hunter",
		Email:     "johnhunter@hunterinformatics.com",
		City:      "Pittsburgh",
		State:     "PA",
	}
}

func (errorReader) Read(p []byte) (int, error) {
	return 0, errors.New("test error")
}
