package main

import "testing"

func TestParseMailAcceptedAddress(t *testing.T) {
	actual := validateEmail("John Oliver <JohnOliver@hbo.com>")
	if false != actual {
		fail(t, false, actual)
	}
}

func TestBadEmail(t *testing.T) {
	actual := validateEmail("fake")
	if false != actual {
		fail(t, false, actual)
	}
}

func TestGoodEmail(t *testing.T) {
	actual := validateEmail("admin@localhost")
	if true != actual {
		fail(t, true, actual)
	}
}

func TestShortPassword(t *testing.T) {
	actual := validatePassword("Sh0rt!")
	if false != actual {
		fail(t, false, actual)
	}
}

func TestNoNumPassword(t *testing.T) {
	actual := validatePassword("AlmostGood!")
	if false != actual {
		fail(t, false, actual)
	}
}

func TestNoUpperPassword(t *testing.T) {
	actual := validatePassword("stilln0tgood!")
	if false != actual {
		fail(t, false, actual)
	}
}

func TestNoSpecialPassword(t *testing.T) {
	actual := validatePassword("iRe4llySuck")
	if false != actual {
		fail(t, false, actual)
	}
}
