package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"time"
)

type Reg struct {
	Password  string
	Email     string
	FirstName string
	LastName  string
	City      string
	State     string
}

func register(w http.ResponseWriter, body []byte) {
	var reg Reg
	err := json.Unmarshal(body, &reg)
	if err != nil {
		http.Error(w, buildJsonError("invalid json request"), 400)
		return
	}
	if !validateEmail(reg.Email) {
		http.Error(w, buildJsonError("invalid Email"), 400)
		return
	}
	if reg.FirstName == "" || reg.LastName == "" {
		http.Error(w, buildJsonError("invalid Name"), 400)
		return
	}
	if !validatePassword(reg.Password) {
		http.Error(w, buildJsonError("invalid Password"), 400)
		return
	}
	if reg.City == "" {
		http.Error(w, buildJsonError("invalid City"), 400)
		return
	}
	if reg.State == "" {
		http.Error(w, buildJsonError("invalid State"), 400)
		return
	}

	err = db.QueryRow("SELECT Email FROM users where email=?", reg.Email).Scan(&reg.Email)
	if err != sql.ErrNoRows {
		http.Error(w, buildJsonError("user already exists"), 400)
		return
	}

	err = db.QueryRow("SELECT Email FROM businesses where Email=?", reg.Email).Scan(&reg.Email)
	if err != sql.ErrNoRows {
		http.Error(w, buildJsonError("a business account already exists with this Email"), 400)
		return
	}

	statement, err := db.Prepare("INSERT INTO users (email, password, firstname, lastname, city, state) VALUES (?, ?, ?, ?, ?, ?)")
	if err != nil {
		log.Println("database error: ", err)
		http.Error(w, buildJsonError("internal server error"), 500)
		return
	}

	hash := sha256.New()
	hash.Write([]byte(reg.Password))
	reg.Password = fmt.Sprintf("%x", hash.Sum(nil))

	statement.Exec(reg.Email, reg.Password, reg.FirstName, reg.LastName, reg.City, reg.State)

	fmt.Fprintf(w, "{\"success\": true, ")

	// Register sessions

	row := db.QueryRow("SELECT id FROM users WHERE email=?", reg.Email)
	var id int
	row.Scan(&id)

	//Set jwt
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		id:          id,
		accountType: "user",
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, _ := token.SignedString(jwtKey)

	fmt.Fprintf(w, "\"token\": \""+tokenString+"\"}")
}
