package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

func buildJsonError(error string) string {
	return "{\"error\": \"" + error + "\"}"
}

func handleBusinessCommands(w http.ResponseWriter, body []byte, apiParts []string, r *http.Request) {
	if len(apiParts) == 1 {

		if r.Method == "GET" {
			keys := r.URL.Query()

			limit := maxBusinesses
			if len(keys["limit"]) >= 1 {
				var err error
				limit, err = strconv.Atoi(keys["limit"][0])
				if err != nil {
					limit = maxBusinesses
				}
			}

			offset := 0
			if len(keys["offset"]) >= 1 {
				var err error
				offset, err = strconv.Atoi(keys["offset"][0])
				if err != nil {
					offset = 0
				}
			}

			search := ""
			if len(keys["search"]) >= 1 {
				search = keys["search"][0]
			}

			cat := 0
			if len(keys["cat"]) >= 1 {
				var err error
				cat, err = strconv.Atoi(keys["cat"][0])
				if err != nil {
					cat = 0
				}
			}

			listBusinesses(w, limit, offset, search, cat)
			return
		} else if r.Method == "POST" {
			createBusiness(w, body)
			return
		}

	} else if len(apiParts) == 3 {
		id, err := strconv.Atoi(apiParts[2])
		if err != nil {
			http.Error(w, buildJsonError("invalid id"), 400)
			return
		}

		if r.Method == "GET" {
			listBusiness(w, id, true)
			return
		} else if r.Method == "DELETE" {
			deleteBusiness(w, id)
			return
		} else if r.Method == "PUT" {
			editBusiness(w, id)
			return
		}
	}

	http.Error(w, buildJsonError("invalid api command"), 400)
	return
}

func handleUserCommands(w http.ResponseWriter, r *http.Request, body []byte, apiParts []string) {
	if len(apiParts) == 1 {
		http.Error(w, buildJsonError("not enough arguments"), 400)
		return

	} else if len(apiParts) == 2 {
		switch apiParts[1] {
		case "login":
			login(w, body)
			return
		case "register":
			register(w, body)
			return
		case "me":
			getCurrentProfile(w, r)
			return
		}

	} else if len(apiParts) == 3 {
		if apiParts[1] == "id" {
			id, err := strconv.Atoi(apiParts[2])
			if err != nil {
				http.Error(w, buildJsonError("invalid id"), 400)
				return
			}
			getProfileById(w, id)
			return
		}
	}

	http.Error(w, buildJsonError("invalid api command"), 400)
	return
}

func handleAPI(w http.ResponseWriter, r *http.Request, path string) {
	// Get request body
	if r.Body == nil { // body should never be nil. Empty body should be \0
		http.Error(w, buildJsonError("bad body"), 500)
		return
	}
	body, e := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if e != nil {
		fmt.Fprintf(w, buildJsonError(e.Error()))
		return
	}

	//--------API router---------

	apiParts := strings.Split(strings.Trim(path, "/"), "/")
	if len(apiParts) == 1 {
		http.Error(w, buildJsonError("no api command provided"), 400)
		return
	}

	apiParts = apiParts[1:]

	if apiParts[0] == "business" {
		handleBusinessCommands(w, body, apiParts, r)
	} else if apiParts[0] == "user" {
		handleUserCommands(w, r, body, apiParts)
	} else if apiParts[0] == "post" {
		post(w, r, body)
	} else {
		fmt.Fprintf(w, buildJsonError("invalid api command"))
		return
	}
}
