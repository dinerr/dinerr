package main

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
)

type Post struct {
	ImgSrc      string
	Description string
}

func post(w http.ResponseWriter, r *http.Request, body []byte) {
	var post Post
	err := json.Unmarshal(body, &post)
	if err != nil {
		http.Error(w, buildJsonError("invalid json request"), 400)
		return
	}
	var tokenString = r.Header.Get("x-access-token")
	if tokenString == "" {
		http.Error(w, buildJsonError("bad token"), 400)
	}
	var claims = &Claims{}
	var token *jwt.Token
	token, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		http.Error(w, buildJsonError(err.Error()), 400)
		return
	}

	if !token.Valid {
		http.Error(w, buildJsonError("invalid token"), 400)
		return
	}
	if claims.accountType == "user" {
		http.Error(w, buildJsonError("expected business account"), 400)
		return
	}

	statement, err := db.Prepare("INSERT INTO posts (imgsrc, businessid, description, createdat, updatedat) VALUES (?, ?, ?, ?, ?)")
	statement.Exec(post.ImgSrc, claims.id, post.Description, time.Now().Unix(), time.Now().Unix())
}
