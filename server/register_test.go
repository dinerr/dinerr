package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"
)

func TestBadEmailResponse(t *testing.T) {
	reg := generateGoodReg()
	reg.Email = "badEmail"
	body, err := json.Marshal(reg)
	if err != nil {
		t.Fatal(err.Error())
	}

	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "invalid Email"
	if actual != expected {
		fail(t, expected, actual)
	}
}

func TestBadBodyRegister(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(emptyByte))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "unexpected end of JSON input"
	if actual != expected {
		fail(t, expected, actual)
	}
}

func TestBadPasswordRegister(t *testing.T) {
	reg := generateGoodReg()
	reg.Password = "badpass"
	body, err := json.Marshal(reg)
	if err != nil {
		t.Fatal(err.Error())
	}

	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "invalid Password"
	if actual != expected {
		fail(t, expected, actual)
	}
}

func TestBadFirstNameRegister(t *testing.T) {
	reg := generateGoodReg()
	reg.FirstName = ""
	body, err := json.Marshal(reg)
	if err != nil {
		t.Fatal(err.Error())
	}

	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "invalid Name"
	if actual != expected {
		fail(t, expected, actual)
	}
}

func TestBadLastNameRegister(t *testing.T) {
	reg := generateGoodReg()
	reg.LastName = ""
	body, err := json.Marshal(reg)
	if err != nil {
		t.Fatal(err.Error())
	}

	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "invalid Name"
	if actual != expected {
		fail(t, expected, actual)
	}
}

func TestBadCityRegister(t *testing.T) {
	reg := generateGoodReg()
	reg.City = ""
	body, err := json.Marshal(reg)
	if err != nil {
		t.Fatal(err.Error())
	}

	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "invalid City"
	if actual != expected {
		fail(t, expected, actual)
	}
}

func TestBadStateRegister(t *testing.T) {
	reg := generateGoodReg()
	reg.State = ""
	body, err := json.Marshal(reg)
	if err != nil {
		t.Fatal(err.Error())
	}

	req, err := http.NewRequest(http.MethodPost, "/api/user/register", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err.Error())
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	actual := p.(map[string]interface{})["error"]
	expected := "invalid State"
	if actual != expected {
		fail(t, expected, actual)
	}
}
