package main

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

func getProfileById(w http.ResponseWriter, id int) {
	var firstName, lastName, city, state string
	err := db.QueryRow("SELECT firstname, lastname, city, state FROM users where id = ?", id).Scan(&firstName, &lastName, &city, &state)
	if err != nil {
		http.Error(w, buildJsonError("user not found"), 400)
		return
	}

	fmt.Fprintf(w, "{\"firstname\": \""+firstName+"\",\r\n"+
		"\"lastname\": \""+lastName+"\",\r\n"+
		"\"city\": \""+city+"\",\r\n"+
		"\"state\": \""+state+"\"}")

	return
}

func getCurrentProfile(w http.ResponseWriter, r *http.Request) {
	var tokenString string = r.Header.Get("x-access-token")
	if tokenString == "" {
		http.Error(w, buildJsonError("bad token"), 400)
	}
	var claims *Claims = &Claims{}
	var err error
	var token *jwt.Token
	token, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		http.Error(w, buildJsonError(err.Error()), 400)
		return
	}

	if !token.Valid {
		http.Error(w, buildJsonError("invalid token"), 400)
		return
	}
	if claims.accountType == "business" {
		http.Error(w, buildJsonError("expected user account"), 400)
		return
	}

	getProfileById(w, claims.id)
	return
}
