package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"
)

func TestNoAPICommand(t *testing.T) {
	req, err := http.NewRequest("GET", "/api", bytes.NewBuffer(emptyByte))
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "no api command provided"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}

func TestUserCommandTooShort(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/user", bytes.NewBuffer(emptyByte))
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "not enough arguments"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}

func TestBadTopLevelAPICommand(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/bad", bytes.NewBuffer(emptyByte))
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "invalid api command"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}

func TestBadUserAPICommand(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/user/bad", bytes.NewBuffer(emptyByte))
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "invalid api command"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}

func TestBadBusinessAPICommand(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/business/bad", bytes.NewBuffer(emptyByte))
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "invalid api command"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}

func TestAPIBadBody(t *testing.T) {
	req, err := http.NewRequest("GET", "/api", nil)
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "bad body"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}

func TestAPIReaderFail(t *testing.T) {
	req, err := http.NewRequest("GET", "/api", errorReader(0))
	if err != nil {
		t.Fatal(err)
	}

	response := executeRequest(req)

	var p interface{}
	err = json.Unmarshal(response.Body.Bytes(), &p)
	if err != nil {
		t.Fatal(err)
	}

	expected := "test error"
	actual := p.(map[string]interface{})["error"]
	if expected != actual {
		fail(t, expected, actual)
	}
}
