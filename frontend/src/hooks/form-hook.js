import { useState } from 'react';

const useLoginForm = (state, callback) => {
	
	const [inputs, setInputs] = useState(state);
	
	const handleSubmit = (event) => {
		const form = event.currentTarget;
		if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		}
		callback();
  };

	const handleInputChange = event => {
		event.persist();
		setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
	};

	return {
		inputs,
		handleSubmit,
		handleInputChange
	};
};

export default useLoginForm;
