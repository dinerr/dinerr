import request from './request';

function register(data) {
	return request({
			url: '/business/register',
			method: 'POST',
			data
	});
}

function login(credentials) {
	return request({
		url: '/business/login',
		method: 'POST',
		data: credentials
	});
}

function fetchAll() {
	return request({
		url: '/business',
		method: 'GET'
	});
}

const BusinessService = {
	register, login, fetchAll
}

export default BusinessService;