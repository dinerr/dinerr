import request from './request';
import setAuthToken from '../utils/setAuthToken';
import store from '../store';
import { setCurrentUser } from '../actions/authActions';

function register(userData) {
	return request({
			url: '/user/register',
			method: 'POST',
			data: userData
	});
}

function login(credentials) {
	return request({
		url: '/user/login',
		method: 'POST',
		data: credentials
	});
}

function logout(){
	localStorage.removeItem("jwtToken");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
	store.dispatch(setCurrentUser({}));
	window.location.href = "/";
}

const UserService = {
	register, login, logout
}

export default UserService;