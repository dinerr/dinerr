import React, {Component} from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import "./App.css";

import Header from "./components/layout/Header";
import Footer from "./components/layout/Footer";
import PrivateRoute from "./components/common/PrivateRoute";
import store from "./store";
import jwt_decode from 'jwt-decode';

// pages
import Login from "./components/auth/Login";
import NewUser from "./components/auth/NewUser";
import NewBusiness from "./components/auth/NewBusiness";
import Feed from "./components/feed/Feed";
import PageNotFound from "./components/page-not-found/PageNotFound";
import Dashboard from "./components/dashboard/Dashboard";
import setAuthToken from "./utils/setAuthToken";
//import InfoBox from "./components/feed/InfoBox";
// import { InfoBoxBody } from "./components/feed/InfoBox";
import { setCurrentUser, clearCurrentProfile } from './actions/authActions'
import UserService from './services/UserService';

const token = localStorage.getItem('jwtToken');

if (token) {
	setAuthToken(token);

	// decode
	const decoded = jwt_decode(token);
	store.dispatch(setCurrentUser(decoded));

	const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    UserService.logout();
    store.dispatch(clearCurrentProfile());
    // Redirect to login
    window.location.href = "/login";
	}
}

//function App() {
class App extends Component{


	constructor(props){
		super(props)
		this.state={
			restaurantProfile:
			[
			]
		}
	}


	setUser(){
		this.setState({
			restaurantProfile:[
				{
				name: "Luke Smutny",
				age: "21",
				description: "Hello we are Luke Smutnys Restaurant",
				},
				...this.state.users,
			]
		})
	}
	//let name = "Luke Smutny"
	render(){
	return (
		<Provider store={store}>
			<Router>
				<div className="App">
					<Header />
					<Switch>
						<Route exact path="/dashboard" component={Dashboard} />
						<Route path="/login" component={Login} />
						<Route path="/new-user" component={NewUser} />
+						<Route path="/new-business" component={NewBusiness} />
						{/* <PrivateRoute path="/feed" component={<Feed name={restName} src={imgSrc} date={date} description={text} />} /> */}
						<PrivateRoute path="/feed" component={Feed} />
						<Route exact path="/" component={Dashboard} />
						<Route component={PageNotFound} />
					</Switch>
					<Footer />
				</div>
			</Router>
		</Provider>
	);
	}
}

export default App;
