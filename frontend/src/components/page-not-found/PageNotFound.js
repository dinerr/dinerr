import React from 'react';
import { Container } from 'react-bootstrap';

const PageNotFound = () => (
  <Container>
    <h2>This page wasn't found.</h2>
    <p>Try something else. 404</p>
  </Container>
);

export default PageNotFound;