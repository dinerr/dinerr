import React from "react";
import { Link } from "react-router-dom";
import { Button, Col, Row } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
import { connect } from "react-redux";
import UserService from "../../services/UserService";

const Header = props => (
	<header className="App-header">
		<Link to="/">
			<h1>Dinerr</h1>
		</Link>
		<Menu right>
			{props.auth.isAuthenticated ? (
				<Button
					onClick={UserService.logout}
					type="button"
					variant="outline-light"
				>
					Log out
				</Button>
			) : (
				<>
					<Link to="/new-user">
						<Button type="button" variant="link">
							New User
						</Button>
					</Link>
					<Link to="/new-business">
						<Button type="button" variant="link">
							New Business
						</Button>
					</Link>
					<Link to="/login">
						<Button type="button" variant="outline-light">
							Log In
						</Button>
					</Link>
				</>
			)}
		</Menu>
	</header>
);

const mapStateToProps = state => ({
	auth: state.auth
});

export default connect(mapStateToProps)(Header);
