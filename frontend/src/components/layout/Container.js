import React from 'react';
import { Container } from 'react-bootstrap';
import { string, element } from 'prop-types';

const CustomContainer = (props) => (
	<Container fluid className={props.className} style={{ padding: 0, minHeight: '60vh' }}>
		{props.children}
	</Container>
);

CustomContainer.propTypes = {
	className: string,
	children: element.isRequired
}

CustomContainer.defaultProps = {
	className: ""
}

export default CustomContainer;