import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import MapViewer from "../common/Mapviewer";

const Footer = () => (
  <footer className="App-footer">
    <Container fluid>
      <Row>
        <Col>
          <p style={{ fontFamily: "Barriecito", fontSize: "2rem" }}>
            A Pittsburgh Company!
          </p>
          {/* <p>
            4200 Fifth Ave
            <br />
            Pittsburgh, PA 15260
          </p> */}
          <MapViewer />
        </Col>
        <Col>
          tbd - contact form or something
        </Col>
      </Row>
      <Row
        style={{
          background: "#04090a"
        }}
      >
        <Col>Something</Col>
        <Col>&copy; Dinerr {new Date().getFullYear()}</Col>
        <Col>Something</Col>
      </Row>
    </Container>
  </footer>
);

export default Footer;
