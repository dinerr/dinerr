import React, { Component } from "react";
//import Container from 'react-bootstrap/Container'
import { InfoBoxBody } from "./InfoBox.js"; //'./components/feed/InfoBox.js'
import RightColumn from "./FeedColumn.js";
//import { FeedBody } from './components/feed/feedbody.js'
import { Col } from "react-bootstrap";
import { Container, Row } from "react-bootstrap";
import BusinessService from "../../services/BusinessService";

// this will be pulled from db eventually
const tmpData = [
	{
		id: 1,
		name: "Luke's Lobster",
		imageSrc: "https://www.aspcapetinsurance.com/media/1064/mountain-dog.jpg",
		description:
			"Hello everyone, we want to update you on our newest menu item, lobster salads with mango.  Come in soon and enjoy!",
		date: "7/19/2019"
	},
	{
		id: 2,
		name: "Luke's Lobster",
		imageSrc: "https://www.aspcapetinsurance.com/media/1064/mountain-dog.jpg",
		description:
			"Hello everyone, we want to update you on our newest menu item, lobster salads with mango.  Come in soon and enjoy!",
		date: "7/19/2019"
	},
	{
		id: 3,
		name: "Luke's Lobster",
		imageSrc: "https://www.aspcapetinsurance.com/media/1064/mountain-dog.jpg",
		description:
			"Hello everyone, we want to update you on our newest menu item, lobster salads with mango.  Come in soon and enjoy!",
		date: "7/19/2019"
	}
];

class Feed extends Component {
	//follow from the other one
	//where should the whole rendering go, in APP.js???
	//This is the feed, the first "box"
	//The "tweet.js" is what formats the data from
	//the api
	//The data from api is gotten in App.js...

	constructor(props) {
		super(props);
		this.state = {};
	}

	componentDidMount() {
		BusinessService.fetchAll()
			.then(response => {
				this.setState({
					business: response.data.businesses
				});
			})
			.catch(err => {
				console.log(err);
			});
	}

	render() {
		return (
			<Container className="Feed">
				{tmpData.map(item => (
					<InfoBoxBody
						key={item.id}
						name={item.name}
						src={item.imageSrc}
						date={item.date}
						description={item.description}
					/>
				))}
			</Container>
		);
	}
}

export default Feed;
