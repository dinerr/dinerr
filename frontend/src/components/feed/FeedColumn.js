import React from 'react';
import { Container, Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
//import Dog from './dog'


const RightColumn = () => (
    <Col className="feed-column">
        <h2>Here are all of your links/subscriptions</h2>
        <Button variant="primary" type="submit">
        Sign up!
        </Button>
  </Col>
);

export default RightColumn;