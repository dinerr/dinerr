import React from 'react';
import { Container, Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
//import Dog from './dog'


const rightColumn = () => (
    <Col className="feed-column">
        <h2>Here are all of your links/subscriptions</h2>
        <Button variant="primary" type="submit">
        Sign up!
        </Button>
  </Col>
);





//box that holds the information
const InfoBox = (props) => {
    return (
        <div className="info-body">
            {props.children} 
        </div>
    )
}

//image that will be used
 const Image = (props) => {
    return(
        <img src={props.src} alt="TheDog" className="picture">
        </img>
        //<div className="Test">Luke is hungry</div>
    )
} 
 
//Name of the restaurant
const Name = (props) =>{
    return(
        <div className="name">{props.name}</div>
    )
}

const Date = (props) =>{
    return(
        <div className="date">{props.date}</div>
    )
}

const Description = (props) =>{
    return(
        <div className="description">{props.description}</div>
    )
}



const InfoBoxBody = (props) => {
    return(
        <InfoBox>            
            <div className="inner-body"> 
                <Image src={props.src} />
                <div className="body">
                    <div className="inner-body">
                    <Name name={props.name} />
                    <Date date = {props.date} />
                </div>                     
                <Description description = {props.description} />
                </div>
            </div>
        </InfoBox>
        
        
        
    )
}

export { InfoBoxBody }