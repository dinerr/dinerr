import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import "bootstrap/dist/css/bootstrap.min.css";
import validator from "validator";
//import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { Form, Button } from "react-bootstrap";
import Container from "../layout/Container";
import UserService from "../../services/UserService";
import setAuthToken from '../../utils/setAuthToken';
import { setCurrentUser } from '../../actions/authActions';

const formDefaults = {
  email: { value: "", isValid: true, message: "" },
  password: { value: "", isValid: true, message: "" },
  confirmPassword: { value: "", isValid: true, message: "" },
  firstName: { value: "", isValid: true, message: "" },
  lastName: { value: "", isValid: true, message: "" },
  city: { value: "", isValid: true, message: "" },
  state: { value: "", isValid: true, message: "" }
};

class NewUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ...formDefaults,
      validated: false,
      formError: null
    };
  }

  formIsValid = () => {

    const email = { ...this.state.email };
    const password = { ...this.state.password };
    const city = { ...this.state.city };
    const state = { ...this.state.state };
    const lastName = { ...this.state.lastName };
    const firstName = { ...this.state.firstName };

    const confirmPassword = { ...this.state.confirmPassword };
    let isGood = false;

    if (!validator.isEmail(email.value)) {
      email.isValid = false;
      email.message = "That is not a valid email.";
    }

    this.setState({
      email,
      password,
      firstName,
      lastName,
      city,
      state
    });
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: {...this.state[event.target.id], value: event.target.value }
    });
  };

  handleSubmit = event => {
    const form = event.currentTarget;

    // if (form.checkValidity() === false) {
    event.preventDefault();
    event.stopPropagation();
    // }

    this.setState({ validated: true });

    const userObj = {
      email: this.state.email.value,
      password: this.state.password.value,
      firstName: this.state.firstName.value,
      lastName: this.state.lastName.value,
      city: this.state.city.value,
      state: this.state.state.value
    };

    if (form.checkValidity()) {
      UserService.register(userObj)
        .then(response => {
          console.log(response);
          if (response && response.success) {
            setAuthToken(response.token);
            localStorage.setItem("jwtToken", response.token);
            const decoded = jwt_decode(localStorage.getItem("jwtToken"));
            this.props.setCurrentUser(decoded);
          }
        })
        .catch(err => {
          console.error(err);
          const errorMessage = err && err.data && err.data.error;
          if (errorMessage) {
            this.setState({ formError:  errorMessage });
          }
        });
    }
  };

  render() {
    const { validated } = this.state;
    if (this.props.auth.isAuthenticated) {
      return <Redirect to='/feed' />;
    }
    return (
      <Container>
        <div className="SignUp">
          <h2>Sign Up</h2>
          <Form
            onSubmit={evt => this.handleSubmit(evt)}
            noValidate
            validated={validated}
          >
            {/* first name */}
            <Form.Group controlId="firstName">
              <Form.Control
                autoFocus
                type="text"
                required
                placeholder="First name"
                value={this.state.firstName.value}
                onChange={this.handleChange}
              />
              <Form.Control.Feedback type="invalid">
                {this.state.firstName.message || 'First name is invalid.'}
              </Form.Control.Feedback>
            </Form.Group>

            {/* last name */}
            <Form.Group controlId="lastName">
              <Form.Control
                autoFocus
                type="text"
                required
                placeholder="Last name"
                value={this.state.lastName.value}
                onChange={this.handleChange}
              />
              <Form.Control.Feedback type="invalid">
                {this.state.password.message || 'Last name is invalid.'}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="email">
              <Form.Control
                autoFocus
                type="email"
                placeholder="Email"
                required
                value={this.state.email.value}
                onChange={this.handleChange}
              />
              <Form.Control.Feedback type="invalid">
                {this.state.email.message}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Control
                placeholder="Password"
                value={this.state.password.value}
                required
                onChange={this.handleChange}
                type="password"
              />
              <Form.Control.Feedback type="invalid">
                {this.state.password.message || 'Password is invalid.'}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="confirmPassword">
              <Form.Control
                placeholder="Confirm Password"
                value={this.state.confirmPassword.value}
                required
                onChange={this.handleChange}
                type="password"
              />
              <Form.Control.Feedback type="invalid">
                {this.state.password.message || 'Confirm password is invalid.'}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="city">
              <Form.Control
                placeholder="City"
                value={this.state.city.value}
                onChange={this.handleChange}
                type="city"
                required
              />
              <Form.Control.Feedback type="invalid">
                {this.state.password.message || 'City is invalid.'}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="state">
              <Form.Control
                placeholder="State"
                required
                value={this.state.state.value}
                onChange={this.handleChange}
                type="state"
              />
              <Form.Control.Feedback type="invalid">
                {this.state.password.message || 'State is invalid.'}
              </Form.Control.Feedback>
            </Form.Group>
            <p>{this.state.formError}</p>
            <Button variant="primary" type="submit">
              Sign Up
            </Button>
          </Form>
          <p>
            <Link to="/login">Already have an account? Login</Link>
          </p>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { setCurrentUser })(NewUser);
