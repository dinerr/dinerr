import React, { Component, useState } from "react";
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import { setCurrentUser } from '../../actions/authActions';

import "bootstrap/dist/css/bootstrap.min.css";
//import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { Form, Button } from "react-bootstrap";
import Container from '../layout/Container';
import "./Login.css";
import UserService from "../../services/UserService";
import setAuthToken from "../../utils/setAuthToken";

function Login(props) {

  const [validated, setValidated] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [formError, setFormError] = useState("");
  const [feedRedirect, setFeedRedirect] = useState(false);
  const [loginRedirect, setLoginRedirect] = useState(false);

  const handleSubmit = event => {
    const form = event.currentTarget;

    setFormError('');

    event.preventDefault();
    event.stopPropagation();

    setValidated(true);

    if (form.checkValidity()) {
      UserService.login({ email, password }).then(response => {
        console.log(response);
        if (response && response.success) {
          setAuthToken(response.token);
          localStorage.setItem("jwtToken", response.token);
          const decoded = jwt_decode(localStorage.getItem("jwtToken"));
          props.setCurrentUser(decoded);
        }
      }).catch(err => {
        setFormError(err.data.error);
        console.log(err);
      });
    }

  };

  if (props.auth.isAuthenticated) {
    return <Redirect to='/feed' />;
  }

  if (loginRedirect) {
    return <Redirect to='/login' />;
  }

return (
    <Container>
      <div className="Login">
        <span>Log In</span>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Form.Group controlId="email1">
            <Form.Control
              autoFocus
              value={email}
              required
              onChange={e => setEmail(e.target.value)}
              type="email"
              placeholder="email"
            />
            <Form.Control.Feedback type="invalid">
              Email is invalid.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group controlId="password1">
            <Form.Control
              placeholder="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              type="password"
              required
            />
            <Form.Control.Feedback type="invalid">
              Password is invalid.
            </Form.Control.Feedback>
          </Form.Group>
          <p style={{ color: 'red' }}>{formError}</p>
          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
        <Button variant="link" onClick={() => setLoginRedirect(true)}>Don't have an account yet? Sign up!</Button>
      </div>
    </Container>
  );

}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { setCurrentUser })(Login);

