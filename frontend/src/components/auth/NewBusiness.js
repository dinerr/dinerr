import React, { Component, useState } from "react";
import { Link, Redirect } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
//import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { Form, Button } from "react-bootstrap";
import Container from "../layout/Container";
import BusinessService from '../../services/BusinessService';

function NewBusiness() {

  const [validated, setValidated] = useState(false);
  const [formError, setFormError] = useState("");
  const [feedRedirect, setFeedRedirect] = useState(false);

  // form fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [name, setName] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [zip, setZip] = useState("");

  const handleSubmit = event => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    setValidated(true);
    if (form.checkValidity()) {
      BusinessService.register({
        email, password, name, city, state, zip
      }).then(response => {
        console.log(response);
        if (response && response.success) {
          setFeedRedirect(true);
        }
      }).catch(err => {
        setFormError(err.data.error);
        console.log(err);
      });
    }
  };

  // if (feedRedirect) {
  //   return <Redirect to='/feed' />;
  // }

  return (
    <Container>
      <div className="SignUp">
          <span>Business Sign Up</span>
          <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Form.Group controlId="name">
              <Form.Control
                autoFocus
                required
                type="text"
                placeholder="Business Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="email2">
              <Form.Control
                autoFocus
                required
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Control
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password"
              />
            </Form.Group>
            <Form.Group controlId="confirmPassword">
              <Form.Control
                placeholder="Confirm password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                type="password"
              />
            </Form.Group>
            <Form.Group controlId="city">
              <Form.Control
                placeholder="City"
                value={city}
                onChange={(e) => setCity(e.target.value)}
                type="text"
              />
            </Form.Group>
            <Form.Group controlId="state">
              <Form.Control
                placeholder="State"
                value={confirmPassword}
                onChange={(e) => setState(e.target.value)}
                type="text"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Sign Up
            </Button>
          </Form>
          <p><Link to='/login'>Already have an account? Login</Link></p>
        </div>
    </Container>
  );

}









// class NewBusiness extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {};
//   }
//   render() {
//     return (
//       <Container>
//         <div className="SignUp">
//           <span>Business Sign Up</span>
//           <Form onSubmit={this.handleSubmit}>
//             <Form.Group controlId="email2">
//               <Form.Control
//                 autoFocus
//                 type="email"
//                 placeholder="email"
//                 value={this.state.email2}
//                 onChange={this.handleChange}
//               />
//             </Form.Group>
//             <Form.Group controlId="password2">
//               <Form.Control
//                 placeholder="password"
//                 value={this.state.password2}
//                 onChange={this.handleChange}
//                 type="password"
//               />
//             </Form.Group>
//             <Button variant="primary" type="submit">
//               Sign Up
//             </Button>
//           </Form>
//           <p><Link to='/login'>Already have an account? Login</Link></p>
//         </div>
//       </Container>
//     );
//   }
// }

export default NewBusiness;
