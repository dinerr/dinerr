import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import { Col } from "react-bootstrap";
import { Link } from 'react-router-dom';

class RightColumn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginRedirect: false
    };
  }
  redirect = () => {
    this.setState({ loginRedirect: true });
  }

  render() {
    if (this.state.loginRedirect) {
      return <Redirect to='/login' />;
    }

    return (
      <Col onClick={this.redirect}>
        <div className="right-column">
          <Link to='/new-business'><h2>I'm a new business!</h2></Link>
        </div>
      </Col>
    );
  }
}

export default RightColumn;
