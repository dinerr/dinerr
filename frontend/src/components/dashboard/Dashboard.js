import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import LeftColumn from './LeftColumn';
import RightColumn from './RightColumn';
import Container from '../layout/Container';

function Dashboard(props) {

  if (props.auth.isAuthenticated) {
    return <Redirect to='/feed' />;
  }

  return (
    <Container className="Dashboard">
      <Row noGutters>
        <LeftColumn />
        <RightColumn />
      </Row>
    </Container>
  );
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Dashboard);