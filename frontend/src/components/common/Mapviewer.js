import React from 'react';

const MapViewer = () => (
		<div style={{
			overflow: 'hidden',
			background: 'none!important',
			// float: 'right'
		}}>
			<iframe
				width="500"
				height="400"
				id="gmap_canvas"
				src="https://maps.google.com/maps?q=4200%20Fifth%20Ave%2C%20ittsburgh%2C%20PA%2015260&t=&z=13&ie=UTF8&iwloc=&output=embed"
				frameBorder="0"
				scrolling="no"
				marginHeight="0"
				marginWidth="0" />
		</div>
);

export default MapViewer;